import React from 'react';
import Files from 'react-files'
import './App.css';

 export default class FilesDemo extends React.Component{
   constructor(){
     super();
     this.state={
       files: [],
     }
   }
  onFilesChange = (files)=>{
    // console.log(files)
    this.setState({
      files: files,
    })
  }
 
  onFilesError = (error, file) => {
    console.log('error code ' + error.code + ': ' + error.message)
  }
 
  render() {
    return (
      <div>
      <div className="files">
        <Files
          className='files-dropzone'
          onChange={this.onFilesChange}
          onError={this.onFilesError}
          accepts={['image/png', '.pdf', 'audio/*', '.txt']}
          multiple
          maxFiles={3}
          maxFileSize={10000000}
          minFileSize={0}
          clickable
        >
          <button>
          Drop files here or click to upload
          </button>
        </Files>
      </div>
      <div>
      {this.state.files && this.state.files.map(file =>(
        <div>
         <img src={file.preview && file.preview.url} />
        </div>
      ))}
    </div>
    </div>
    )
  }
 }
